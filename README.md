# Junior CTF

*"I want to hack like in movies"* -- If your kid told you that, but s/he is between 8-16 (approximate!) then, you might be interested by this repository.

This repository contains **challenges for kids**. We don't expect them to know anything about Unix (yet) nor how to code (yet). 

**They must be able to read, write, and count**. And they need access to a computer :)

What does it look like? [Screenshots](https://framagit.org/axellec/juniorctf/wikis/Screenshots)

Those challenges are provided for *educational use only*, to help kids understand computer security and become security aware. For the rest, let's state it briefly, **attackers are baaaaaad**.



## Requirements - current limitations

Your kids will need access to:

- SSH client
- a terminal
- an editor
- John The Ripper (I'll try and remove that requirement later)
- Internet. Several challenges don't strictly require Internet, but it might be helpful to find some documentation.

Current limitation: challenges are in **French** currently. But I'm sure it won't be too difficult to translate! 

## Setting up your own Junior CTF

To setup your own Junior CTF, you need:

- **This repository**. It contains the scenarios & sources for the challenges.
- A **CTF framework** to validate the flags, handle scores. And perhaps hints, teams etc. I personally use [CTFd](https://github.com/CTFd/CTFd). I'm sure you can get it to work with something else.
- **Docker**. Each challenge runs in its own Docker container.

The steps are the following:

1. Setup CTFd. Create an admin account.
2. Clone this repository. `git clone https://framagit.org/axellec/juniorctf.git`
3. `cd juniorctf`
4. `make build`. This builds the Docker images for each challenge. You only need to do this once.
5. `make up`. This starts a Docker container for each challenge. To stop the containers, simple do `make down`
6. **Manually create each challenge on CTFd**. Yes, I'm afraid currently this is manual. It's nothing difficult to do though (mostly copy/paste). This means logging in as CTF administrator, then creating each challenge. For each challenge, you copy the description which is located in  each `./CHALLENGENAME/public/description.LANG.md` (where LANG suits your language), upload needed files if any (they will be in `./CHALLENGENAME/public`), set the number of points for the challenge and enter the flag (provided `./CHALLENGENAME/FLAG`).

## How to add a new challenge

Each challenge has its own directory with:

- `Dockerfile`. To build the Docker image for this challenge. Try and keep the containers **light**, because there are several challenges! If your challenge doesn't need any container, then, you don't need it :)
- `Makefile`. **Mandatory**. Must have target `build` - which will build the Docker image (if you don't need a Docker container, then just make this target do nothing). Other typical targets: `run` spawns a Docker container, `clean` stops and removes the container.
- `FLAG`. **Mandatory**. This file contains the expected flag - that's all. You don't want to have to re-do all challenges to find the flag, I tell you. Even if they are easy.
- `solution.md`. Optional. Explains how to solve this challenge.
- `./public` directory. **Mandatory**. Place in this directory all information you need to provide to the kids to solve the challenge. Make sure not to put anything secret in here!
- `./public/description.fr.md`. **Mandatory**. Describes the scenario for the kids to read. `fr` for French - change for another language.
- Place in `./public` any other file you need to provide. The executable to reverse engineer, an unshadowed password file, images etc.
- The challenge directory (`.`) may contain other files, for example source files used to generate an executable, tests etc. Freely organize the rest of the directory the way that suits you!


## Things I know I need to improve

*If you want to contribute, you are welcome!*

- Provide an import file for CTFd containing all challenges
- Ensure that each kid gets his/her own Docker container, and not a container shared (and possibly "polluted") by other kids...
- Modify Docker image of john 1-5 so that participants don't need to have John The Ripper on their own machine.
- Translate challenges to English, and other languages
- Make this Windows friendly...
- Create a Docker image of Junior CTF?
- Add more challenges :)
- Host an instance of Junior CTF


## License

This is released under the [MIT license](./LICENSE). Feel free to share! (hmm, not to kids, right, because solutions are included in the repository :)


