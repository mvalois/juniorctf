Category: Passwords
Points: 100

# Encore le frigo

Si tu ne l'as pas encore fait, je te conseille de résoudre le "Frigo de Pico" avant celui ci.

[Pico le Croco](http://picolecroco.free.fr) n'était pas content que tu lui piques son champagne. Il a décidé de changer de mot de passe pour quelque chose de vraiment plus compliqué. Vraiment.

Montre à Pico que tu y arrives toujours :)

## Ouvrir la Porte du frigo

On accède à la porte du frigo de Pico via SSH. Tu te rappelles comment faire ?

```
- Nom du frigo: ADRESSE IP
- Port: 2942
- identifiant: pico
```

A toi de trouver le mot de passe !

## Mots de passe

Pico a retenu la lecon et n'a pas choisi un mot de passe figurant au palmares des 500 plus mauvais.

Essaie plutot la liste [Ashley Madison](https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Leaked-Databases/Ashley-Madison.txt). Il s'agit d'une liste de mots de passe assez fréquents qui a été trouvée par un attaquant en 2015. Cet attaquant avait récupéré les mots de passe d'un site en ligne qui s'appelle *Ashley Madison*.

## Ca risque d'etre long

Oui, ca risque d'etre long. **Très long**. Plusieurs heures.
On va essayer d'aller un peu plus vite. Utilise l'option `-t`. Schématiquement, cela permet de tester plusieurs mots de passe en parallèle.
Essaie par exemple avec `-t 128`, sauf si tu constates que ton ordinateur devient trop lent (essaie alors -t 32).

Meme avec cette option, cela va encore etre très long.
Tu auras peut etre besoin d'arreter ton ordinateur entre temps.

Tout n'est pas perdu. Dans ce cas là, **conserve bien le fichier hydra.restore** qui se trouve dans ton répertoire.

Plus tard, quand tu voudras **relancer la recherche là où tu t'es arreté**, lance:

```
$ hydra -R
```


## Rappels pour la vraie vie

- Quand tu as soif, c'est mieux de prendre de l'eau ;)
- C'est **interdit** d'accéder à des machines si on ne t'y a pas autorisé. Non vraiment.
- Attaquer un site ce n'est pas "cool" mais bete et irresponsable. Ce n'est pas parce que quelqu'un utilise des mots de passe pourris que tu as le droit de les utiliser à sa place. 

