SUBDIRS := $(wildcard */.)

build: $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@

.PHONY: build $(SUBDIRS) up down

up:
	./sysadmin/up.sh

down:
	./sysadmin/down.sh

